# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strdup.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alouis <alouis@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/09/17 17:33:11 by alouis            #+#    #+#              #
#    Updated: 2020/09/23 12:03:37 by alouis           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

	section	.text
	global	ft_strdup
	extern	malloc

ft_strdup:
		mov	rcx, -1
		cmp	rdi, 0
		je	error

loop:
		inc	rcx
		cmp	BYTE [rdi + rcx], 0
		jne	loop
		inc	rcx
		push	rdi
		mov	rdi, rcx
		call	malloc
		cmp	rax, 0
		je	error
		pop	rdi
		mov	rcx, -1

copy:
		inc	rcx
		mov	dl, BYTE [rdi + rcx]
		mov	BYTE [rax + rcx], dl
		cmp	dl, 0
		jne	copy
		ret

error:
		mov	rax, 0
		ret
