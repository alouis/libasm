	section	.text
	global	ft_write
	extern	__errno_location

ft_write:					;fd = rdi, str = rsi, len = rdx
		mov rax, 1			;system call for write
		syscall
		cmp	rax, 0
		jl	error
		ret

error:
		neg	rax			;errno = rax * -1
		mov	rcx, rax		;save error code
		push	rcx			;or rcx will be trashed by a call
		call	__errno_location	;returns errno address in rax
		pop	rcx
		mov	[rax], rcx		;error code at errno address
		mov	rax, -1
		ret
