# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strcpy.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alouis <alouis@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/09/15 08:13:28 by alouis            #+#    #+#              #
#    Updated: 2020/09/23 08:19:11 by alouis           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

	global ft_strcpy

ft_strcpy:
		mov	rcx, -1
		cmp	rdi, 0
		je	end
		cmp	rsi, 0
		je	end

copy:
		inc	rcx
		mov	dl, [rsi + rcx]
		mov	[rdi + rcx], dl
		cmp	dl, 0
		jne	copy

end:
		mov	rax, rdi
		ret
