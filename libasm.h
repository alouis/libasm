/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libasm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/23 12:06:01 by alouis            #+#    #+#             */
/*   Updated: 2020/09/28 09:25:44 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBASM_H
# define LIBASM_H

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

size_t	ft_strlen(const char *str);
char	*ft_strcpy(char *dest, const char *src);
int	ft_strcmp(const char *s1, const char *s2);
ssize_t	ft_write(int fd, const void *str, size_t len);
ssize_t	ft_read(int fd, void *buf, size_t nbyte);
char 	*ft_strdup(const char *s);

#define STRLEN(x)		printf("|%s|= %d => %d\n", x, (int)strlen(x), (int)ft_strlen(x));
#define	STRCMP(x, y)		printf("%d => %d\n", strcmp(x, y), ft_strcmp(x, y))
#define	WRITE(x)		printf("write (errno) : %ld (%d) => %ld (%d)\n", ft_write(1, x, strlen(x)), errno, write(1, x, strlen(x)), errno);
#define	READ(fd, b, n)		printf("   %ld (%d) = %s\nft %ld (%d) = %s\n", ft_read(fd, b, n), errno, b, read(fd, b, n), errno, b);

#endif
