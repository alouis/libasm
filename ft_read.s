# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_read.s                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alouis <alouis@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/09/17 16:54:39 by alouis            #+#    #+#              #
#    Updated: 2020/09/23 11:18:09 by alouis           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

	section	.text
	global	ft_read
	extern	__errno_location

ft_read:
		mov	rax, 0
		syscall
		cmp	rax, 0
		jl	error
		ret

error:
		neg	rax
		mov	rcx, rax
		push	rcx
		call	__errno_location
		pop	rcx
		mov	[rax], rcx
		mov	rax, -1
		ret
