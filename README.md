# Libasm

Write basic functions in x64 assembly language (in the Intel syntax)

## Linux

Compilation : nasm -felf64 example.s && gcc -no-pie example.o

Calling convention : ft_example

## MacOS

Compilation : nasm -fmacho64 example.s && gcc example.o

Calling convention : _ft_example

MacOS system calls : add 0x2000003 to Linux syscall (write = 0x2000003, read = 0x2000001)


## Ressources

* [NASM Tutorial](https://cs.lmu.edu/~ray/notes/nasmtutorial/)
* [NASM Lessons](https://asmtutor.com/)
* [Register preservation](https://en.wikipedia.org/wiki/X86_calling_conventions#Register_preservation)
* [Linux syscalls](https://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/)
* [Linux syscalls & their code](https://filippo.io/linux-syscall-table/)
* [Relocation error when assembling with gcc](https://stackoverflow.com/questions/46123505/assembling-with-gcc-causes-weird-relocation-error-with-regards-to-data)
* [French tutorial Intel 80x86](https://benoit-m.developpez.com/assembleur/tutoriel/#LIV-C-5)
