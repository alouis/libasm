# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strlen.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alouis <alouis@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/09/14 21:16:51 by alouis            #+#    #+#              #
#    Updated: 2020/09/15 09:28:47 by alouis           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

	global	ft_strlen

ft_strlen:
		mov	rcx, 0			;rcx = 0
		cmp	BYTE [rdi + rcx], 0 	;compare 1st byte of str with 0
		je	end			;end program if comparison is right

compare:
		inc	rcx			;rcx++
		cmp	BYTE [rdi + rcx], 0	;compare (1 + rcx)th byte of str with 0
		jne	compare			;keep calling compare til comparison is right

end:
		mov	rax, rcx		;copy rcx value in rax
		ret				;automatically return rax
