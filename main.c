/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/14 21:17:13 by alouis            #+#    #+#             */
/*   Updated: 2020/09/28 09:10:27 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libasm.h"

void	test_len_and_copy(void)
{
	char	dest[10];
	char	tsed[10];

	printf("\n-- STRLEN --\n");
	STRLEN("");
	STRLEN("four");
	STRLEN("\tfive");

	printf("\n-- STRCPY --\n");
	printf("src = |%s|\ncpy = |%s|\n\n", strcpy(dest, ""),
			ft_strcpy(tsed, ""));
	printf("src = |%s|\ncpy = |%s|\n\n", strcpy(dest, "example"),
			ft_strcpy(tsed, "example"));
}

void	test_cmp_and_dup(void)
{
	char	*dup;
	char	*str = "helloooooo";
	char	*cpy;
	char	*off;

	printf("\n-- STRCMP --\n");
	STRCMP("", "");
	STRCMP("example", "example");
	STRCMP("", " ");
	STRCMP(" ", "");
	STRCMP("example", "xample");
	STRCMP("xample", "example");

	printf("\n-- STRDUP --\n");
	cpy = ft_strdup("");
	off = strdup("");
	printf("|%s| -> |%s| \n", off, cpy);
	if (off)
	{
	   free(cpy);
	   free(off);
	   printf("freed\n");
	}
	cpy = ft_strdup("example");
	off = strdup("example");
	printf("|%s| -> |%s| \n", off, cpy);
	if (off)
	{
	   free(cpy);
	   free(off);
	   printf("freed\n");
	}
	cpy = ft_strdup("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeexample");
	off = strdup("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeexample");
	printf("|%s| -> |%s| \n", off, cpy);
	if (off)
	{
	   free(cpy);
	   free(off);
	   printf("freed\n");
	}

}

void	test_write_and_read(void)
{
	int	fd;
	char	buff[100];

	printf("\n-- WRITE --\n");
	WRITE("\0");
	WRITE("");
	WRITE("MEEEEEEEESSAGE\n");

	printf("\n-- READ --\n");
	printf("errno = %d\n", errno);
	fd = open("Makefile", O_RDONLY);
	printf("errno = %d\n", errno);
	READ(fd, buff, 0);
	READ(fd, buff, 10);
	READ(fd, buff, 25);
	READ(fd, buff, 50);
	READ(fd, buff, 100);
	printf("errno = %d\n", errno);
}

int	main(void)
{
	test_len_and_copy();
	test_write_and_read();
	test_cmp_and_dup();
	return (0);
}
