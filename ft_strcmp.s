# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strcmp.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alouis <alouis@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/09/15 16:19:25 by alouis            #+#    #+#              #
#    Updated: 2020/09/15 16:27:22 by alouis           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

	global ft_strcmp

ft_strcmp:
		mov	rcx, 0
		cmp	rdi, 0			;if s1 is null, cmp w/ s2
		je	null
		cmp	rsi, 0			;if s2 is null, cmp w/ s1
		je	null
		jmp	compare

null:
		cmp	rdi, rsi
		je	end			;s1 = s2 = null

compare:
		mov	dl, BYTE [rdi + rcx]	;copy s1[i] in tmp
		sub	dl, BYTE [rsi + rcx]	;tmp - s2[i]
		jl	less
		jg	more
		;if s1[i] = s2[i], check s1[i++] for end of string
		inc	rcx
		cmp	BYTE [rdi + rcx], 0
		je	null
		jmp	compare

end:
		mov	rax, 0
		ret

less:
		mov	rax, -1
		ret

more:
		mov	rax, 1
		ret
