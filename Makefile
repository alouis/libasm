# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alouis <alouis@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/09/14 21:17:03 by alouis            #+#    #+#              #
#    Updated: 2020/09/28 08:51:23 by alouis           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libasm.a

CC = nasm

FLAG = -felf64

SRCS = ft_strlen.s \
       ft_strcpy.s \
       ft_strcmp.s \
       ft_read.s \
       ft_strdup.s \
       ft_write.s 

OBJS = ${SRCS:.s=.o}

.s.o: ${SRCS}
	${CC} ${FLAG} $<

${NAME}: ${OBJS}
	ar rc ${NAME} ${OBJS}

all: ${NAME}

clean:
	rm ${OBJS}

fclean: clean
	rm ${NAME}

re: fclean all

test: all
	gcc -no-pie main.c *.o && ./a.out
