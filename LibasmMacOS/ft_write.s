	section	.text
	global	_ft_write
	extern	___error

_ft_write:							;fd = rdi, str = rsi, len = rdx
		mov		rax, 0x2000004		;system call for write
		syscall
		jc		error				;carry flag set to 1 if error
		ret

error:
		push	rax					;save error code
		call	___error			;errno address in rax
		pop		QWORD 	[rax]		;error code value at errno address
		mov		rax, -1
		ret
