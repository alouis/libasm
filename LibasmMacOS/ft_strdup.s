	section	.text
	global	_ft_strdup
	extern	_malloc

_ft_strdup:				    			;s1 = rdi
	    mov	    rcx, -1
	    cmp	    rdi, 0
	    je	    error

malloc:
	    inc	    rcx
	    cmp	    BYTE[rdi + rcx], 0
	    jne	    malloc
		inc		rcx
	    push    rdi			    		;push rdi to save s1 
	    mov	    rdi, rcx		    	;because rdi = size needed by malloc
	    call    _malloc
	    cmp	    rax, 0		    		;rax = _malloc(size) = NULL if error)
	    je	    error
	    pop	    rdi			    		;pop rdi to get s1 back & start copying rdi to rax which is the pointer to string malloced
	    mov	    rcx, -1

copy:
	    inc	    rcx,
	    mov	    dl, BYTE[rdi + rcx]
	    mov	    BYTE[rax + rcx], dl
	    cmp	    dl, 0
	    jne	    copy

end:
	    ret

error:
		mov		rax, 0
		ret
