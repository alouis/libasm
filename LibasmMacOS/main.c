/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/14 21:17:13 by alouis            #+#    #+#             */
/*   Updated: 2020/09/23 11:52:31 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/errno.h>

#define	STRLEN(x)		printf("%d -> %d\n", (int)strlen(x), (int)ft_strlen(x));
#define	STRCMP(x, y)		printf("%d -> %d\n", strcmp(x, y), ft_strcmp(x, y));
#define	WRITE(x)		printf("%ld (%d) -> %ld (%d)\n", ft_write(1, x, ft_strlen(x)), errno, write(1, x, ft_strlen(x)), errno);
#define	READ(fd, b, n)		printf("ft %ld (%d) -> %s\n   %ld (%d) -> %s\n", ft_read(fd, b, n), errno, b, read(fd, b, n), errno, b);

size_t	ft_strlen(const char *str);
char	*ft_strcpy(char *dest, const char *src);
int	ft_strcmp(const char *s1, const char *s2);
ssize_t	ft_write(int fd, const void *str, size_t len);
ssize_t	ft_read(int fd, void *buf, size_t nbyte);
char	*ft_strdup(const char *s1);

int	main(void)
{
	char	dest[10];
	char	tsed[10];
	char	buff[100];
	int	fd;
	char	*cpy;
	char	*off;

	printf("\n-- STRLEN --\n");
	STRLEN("");
	STRLEN("\tfive");
	STRLEN("loooong très très looooooong loooong long longgggggggggggg messaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaag");

	printf("\n-- STRCPY --\n");
	printf("src = |%s|\ncpy = |%s|\n\n", strcpy(dest, ""), ft_strcpy(tsed, ""));
	printf("src = |%s|\ncpy = |%s|\n\n", strcpy(dest, "example"), ft_strcpy(tsed, "example"));

	printf("\n-- STRCMP --\n");
	STRCMP("", "");
	STRCMP("example", "example");
	STRCMP("", " ");
	STRCMP(" ", "");
	STRCMP("example", "xample");
	STRCMP("xample", "example");

	printf("\n-- WRITE --\n");
	WRITE("");
	WRITE("TYPE THis down\n");
	WRITE("TYPETYPETYPETYPE TTYYYYYYYPEE TYPETYEPE TYPEEEEEEEE THis dooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooown\n");

	printf("\n-- READ --\n");
	fd = open("Makefile", O_RDONLY); 
	READ(fd, buff, 0);
	READ(fd, buff, 10);
	READ(fd, buff, 25);
	READ(fd, buff, 50);
	READ(fd, buff, 100);

	printf("\n-- STRDUP --\n");
	cpy = ft_strdup("");
	off = strdup("");
	printf("|%s| -> |%s| \n", off, cpy);
	if (off)
	{
	   free(cpy);
	   free(off);
	   printf("freed\n");
	}
	cpy = ft_strdup("example");
	off = strdup("example");
	printf("|%s| -> |%s| \n", off, cpy);
	if (off)
	{
	   free(cpy);
	   free(off);
	   printf("freed\n");
	}
	cpy = ft_strdup("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeexample");
	off = strdup("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeexample");
	printf("|%s| -> |%s| \n", off, cpy);
	if (off)
	{
	   free(cpy);
	   free(off);
	   printf("freed\n");
	}
	return (0);
}
