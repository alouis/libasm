        section .text
		global  _ft_read
		extern	___error

_ft_read:								;fd = rdi, buf = rsi, nbyte = rdx
	    mov		rax, 0x2000003			;system call for read
	    syscall
		jc		error
	    ret

error:
		push	rax
		call	___error
		pop		QWORD [rax]
		mov		rax, -1
		ret
