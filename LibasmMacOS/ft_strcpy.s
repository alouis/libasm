	global	_ft_strcpy
	section	.text

_ft_strcpy:
    		mov rcx, -1				;dest = rdi, src = rsi
		    cmp	rdi, 0
		    je  end
		    cmp rsi, 0
		    je  end

copy:
		    add rcx, 1
			mov dl, [rsi + rcx]
		    mov [rdi + rcx], dl
		    cmp dl, 0
		    jne copy

end:
		    mov rax, rdi
		    ret
