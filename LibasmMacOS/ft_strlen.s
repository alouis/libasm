	section	.text
	global	_ft_strlen

_ft_strlen:
		mov	rcx, 0			;rcx = 0
		cmp	BYTE [rdi + rcx], 0 	;compare 1st byte of str with 0
		je	end			;end program if comparison is right

compare:
		inc	rcx			;rcx++
		cmp	BYTE [rdi + rcx], 0	;compare (1 + rcx)th byte of str with 0
		jne	compare			;keep calling compare til comparison is right

end:
		mov	rax, rcx		;copy rcx value in rax
		ret				;automatically return rax
